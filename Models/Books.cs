﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RazorPages.Models
{
    public class Books
    {
        [Key]
        public int ID { get; set; }
        [Required(ErrorMessage = "Wprowadź tytuł książki!")]
        public string Nazwa { get; set; }
        [Required(ErrorMessage = "Wprowadź Imie i nazwisko autora!")]
        public string Autor { get; set; }
        [Required(ErrorMessage = "Wprowadź numer ISBN!"),StringLength(13)]
        public string ISBN { get; set; }
    }
}
