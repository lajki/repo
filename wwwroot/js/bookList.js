﻿/*var dataTable;

$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {
        dataTable=$('#DT_load').dataTable({

            "ajax": {
                "url": "api/book",
                "type": "GET",
                "datatype": "json"
            },
            "columns": [
                { "data": "nazwa", "width": "20%" },
                { "data": "autor", "width": "20%" },
                { "data": "isbn", "width": "20%" },
                {
                    "data": "id",
                    "render": function (data) {
                        return `<div class="text-center">
                        <a href="/Lista/Edit?id=${data}" class='btn btn-warning text-white' style='cursor:pointer; width:70px;'>
                       Edytuj
                        </a>
                        &nbsp;
                        <a class='btn btn-danger text-white' style='cursor:pointer; width:70px;'
                         onlick=Delete('/api/book?id='+${data})>
                         Delete
                    </a>
                    </div>`;
                }, "width":"40%"
                }
            ],
            "language": {
                "emptyTable": "Brak rekordów"
            },
            "width":"100%"
        });
}*/
var dataTable;

$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {
    dataTable = $('#DT_load').DataTable({
        "ajax": {
            "url": "/api/book",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            { "data": "nazwa", "width": "20%" },
            { "data": "autor", "width": "20%" },
            { "data": "isbn", "width": "20%" },
            {
                "data": "id",
                "render": function (data) {
                    return `<div class="text-center">
                        <a href="/BookList/Edit?id=${data}" class='btn btn-warning text-white' style='cursor:pointer; width:70px;'>
                            Edytuj
                        </a>
                        &nbsp;
                        <a class='btn btn-danger text-white' style='cursor:pointer; width:70px;'
                            onclick=Delete('/api/book?id='+${data})>
                            Usuń
                        </a>
                        </div>`;
                }, "width": "40%"
            }
        ],
        "language": {
            "emptyTable": "Brak rekordów"
        },
        "width": "100%"
    });
}

function Delete(url) {
    swal({
        title: "Jesteś pewien?",
        text: "Po usunięciu nie odzyskasz już danych!",
        icon: "warning", 
        dangerMode: true,
        buttons: {
            confirm: "Tak",
            cancel: "Nie, cofnij"
        }
        

       
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax
                ({
                    type: "DELETE",
                    url: url,
                    success: function (data) {
                        if (data.success) {
                            toastr.success(data.message);
                            dataTable.ajax.reload();
                        }
                    }
                });
        }
        else
        {
            toastr.error(data.message);
        }
    });
}